import Vue from 'vue';
import VueHead from 'vue-head';
import Router from 'vue-router';
import Fragment from 'vue-fragment';
import BootstrapVue from 'bootstrap-vue';
import Home from '@/components/Home/Index';
import Services from '@/components/Services/Index';
import News from '@/components/News/Index';
import About from '@/components/About/Index';
import Contacts from '@/components/Contacts/Index';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(VueHead);
Vue.use(BootstrapVue);
Vue.use(Fragment.Plugin);
Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/services',
      name: 'Services',
      component: Services,
    },
    {
      path: '/news',
      name: 'News',
      component: News,
    },
    {
      path: '/about-company',
      name: 'About',
      component: About,
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: Contacts,
    },
  ],
});
