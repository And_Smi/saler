var gulp=require("gulp"),
    pug=require("gulp-pug"),
    prefix=require("gulp-autoprefixer"),
    imgMin=require("gulp-imagemin"),
    styl=require("gulp-stylus"),
    sync=require("browser-sync"),
    rename=require("gulp-rename"),
    concat=require("gulp-concat"),
    fs = require('fs'),
    data = require('gulp-data'),
    uglJS=require("gulp-uglify"),
    pngQuant=require("imagemin-pngquant"),
    pathRoot = {
        "dev": "./app", // link to dev-dir
        "prod":"./public" // link to pub-dir
    },
    pathSubroot={
        "devFonts":pathRoot.dev+"/src/fonts/**/**",
        "devImg":pathRoot.dev+"/src/img/**/**",
        "devJs":pathRoot.dev+"/src/js/**/**",
        "devStyl":pathRoot.dev+"/src/styl/all.styl",
        "prodFonts":pathRoot.prod+"/fonts/",
        "prodImg":pathRoot.prod+"/img/",
        "prodJs":pathRoot.prod+"/js/",
        "prodCss":pathRoot.prod+"/css/",
    };

// converting styl-files to css
gulp.task("stylus", function(){
    return gulp.src(pathSubroot.devStyl)
        .pipe(styl({
            compress: true,
            'include css': true
        }))
        .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7']))
        .pipe(rename("styles.min.css"))
        .pipe(gulp.dest(pathSubroot.prodCss));
});
// moving font's files from dev-dir to pub-dir
gulp.task("fonts", function(){
    return gulp.src(pathSubroot.devFonts)
        .pipe(gulp.dest(pathSubroot.prodFonts))
});
// optimizing images for prod
gulp.task("img", function(){
    return gulp.src(pathSubroot.devImg)
        .pipe(imgMin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngQuant()]
        }))
        .pipe(gulp.dest(pathSubroot.prodImg))
});
// concat and uglify js for prod
gulp.task("js", function(){
    return gulp.src(pathSubroot.devJs)
        .pipe(uglJS())
        .pipe(gulp.dest(pathSubroot.prodJs))
});
// converting pug-files to html
gulp.task("pug", function(){
    return gulp.src(["app/**/*.pug", "!app/template/**/*.pug", "!app/modules/**/*.pug"])
        .pipe(data(function(file) {
            return JSON.parse(fs.readFileSync('data/data.json', 'utf8'))
        }))
        .pipe(pug({
            pretty: true,
            locals: 'data/data.json'
        }))
        .pipe(gulp.dest(pathRoot.prod));
});
// build a pub-dir
gulp.task("build", gulp.series(
    [
        gulp.parallel("pug", "js", "stylus", "fonts"),
        gulp.parallel("img")
    ]
));
// watching for some changes in dev-dir
gulp.task("watch", function(){
    gulp.watch("./data/data.json", gulp.series("pug"));
    gulp.watch(pathRoot.dev+"src/styl/**/**", gulp.series("stylus", sync.reload));
    gulp.watch(pathSubroot.prodCss, sync.reload);
    gulp.watch(pathRoot.dev+"/**/*.pug", gulp.series("pug"));
    gulp.watch(pathRoot.prod+"**/*.html", sync.reload);
    gulp.watch(pathSubroot.devJs, gulp.series("js"));
    gulp.watch(pathSubroot.prodJs, sync.reload);
});
// starts a server
gulp.task("serve", function(){
    sync({
        server: {
            baseDir: "./public"
        },
        notify: false
    });
    sync.watch("./app", sync.reload);
});
// default task which starts others
gulp.task("default", gulp.series(
    [
        gulp.parallel('stylus', 'js', 'pug'),
        gulp.parallel('serve', 'watch')
    ]
));